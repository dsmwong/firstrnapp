/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {StyleSheet, View, Text, FlatList} from 'react-native';

import Header from './components/Header';
import ConversationItem from './components/conversationItem';

const App = () => {
  const [conversations, setConversations] = useState([
    {
      title: 'Conversation 1',
      preview: 'NAB: This is a preview of the conversation.',
      status: 'New',
      timestamp: '10:25pm',
      tags: ['Tag 1', 'Tag 2'],
    },
    {
      title: 'Conversation 2',
      preview: 'NAB: This is a preview of the conversation.',
      status: 'In progress',
      timestamp: '10:27pm',
      tags: ['Tag 1', 'Tag 2'],
    },
    {
      title: 'Conversation 3',
      preview: 'NAB: This is a preview of the conversation.',
      status: null,
      timestamp: '10:28pm',
      tags: ['Tag 1', 'Tag 2'],
    },
    {
      title: 'Conversation 4',
      preview: 'NAB: This is a preview of the conversation.',
      status: 'New',
      timestamp: '10:29pm',
      tags: ['Tag 1', 'Tag 2'],
    },
    {
      title: 'Conversation 5',
      preview: 'NAB: This is a preview of the conversation.',
      status: 'New',
      timestamp: '10:30pm',
      tags: ['Tag 1', 'Tag 2'],
    },
  ]);

  return (
    <View style={styles.main}>
      <Header title="Message List" />
      <FlatList
        data={conversations}
        keyExtractor={(item) => item.title}
        renderItem={({item}) => <ConversationItem item={item} />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    flex: 1,
    paddingTop: 60,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  text: {
    fontSize: 24,
    fontWeight: '600',
    //color: Colors.black,
  },
  img: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
});

export default App;
