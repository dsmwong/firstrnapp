import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Component = () => {
  return (
    <View>
      <Text style={styles.style1}>some text</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  style1: {
    padding: 15,
  },
});

export default Component;
