import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const ConversationStatus = (props) => {
  let statusColor = styles.redStatus;
  if (props.status === 'In progress') {
    statusColor = styles.orangeStatus;
  }

  return (
    <View style={[styles.conversationStatusView, statusColor]}>
      <Text style={styles.conversationStatus}>{props.status}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  conversationStatusView: {
    borderRadius: 15,
    padding: 1,
    flex: 1,
  },
  conversationStatus: {
    textAlign: 'center',
    color: '#FFFFFF',
  },
  redStatus: {
    backgroundColor: '#F22F46',
  },
  orangeStatus: {
    backgroundColor: '#F47C22',
  },
});

export default ConversationStatus;
