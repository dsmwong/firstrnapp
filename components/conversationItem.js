import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import ConversationStatus from './conversationStatus';

const ConversationItem = (props) => {
  let status = null;
  if (props.item.status) {
    status = <ConversationStatus status={props.item.status} />;
  }

  let tags = [];
  props.item.tags.forEach((tag) => {
    tags.push(
      <View style={styles.conversationTagView}>
        <Text style={styles.conversationTag}>{tag}</Text>
      </View>,
    );
  });

  return (
    <View style={styles.conversationItemView}>
      <View style={styles.conversationTitleRow}>
        <Text style={styles.conversationTitle}>{props.item.title}</Text>
        <Text style={styles.conversationTimestamp}>{props.item.timestamp}</Text>
      </View>

      <View style={styles.conversationBodyRow}>
        <Text style={styles.conversationPreview}>{props.item.preview}</Text>
        {status}
      </View>

      <View style={styles.conversationTagRow}>{tags}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  conversationItemView: {
    width: '100%',
    flex: 1,
    padding: 10,
    borderBottomColor: '#AEB2C1',
    borderBottomWidth: 1,
  },

  conversationTitleRow: {
    flexDirection: 'row',
    padding: 2,
  },
  conversationTitle: {
    flex: 5,
    textAlign: 'left',
    fontSize: 18,
  },
  conversationTimestamp: {
    flex: 1,
    textAlign: 'right',
    color: '#8891AA',
  },

  conversationBodyRow: {
    flexDirection: 'row',
    padding: 2,
    paddingBottom: 20,
  },

  conversationPreview: {
    flex: 3,
  },

  conversationTagRow: {
    flexDirection: 'row',
    padding: 2,
  },
  conversationTagView: {
    backgroundColor: '#F4F4F6',
    borderRadius: 2,
    paddingLeft: 10,
    paddingRight: 10,
  },
  conversationTag: {
    textAlign: 'center',
    color: '#000000',
  },
});

export default ConversationItem;
