# My first attempt at React Native
This is my first attempt at building something out on React Native. Will continue to use this project to test react native concepts. 

## Setup

```
# Boilerplate Sample React Native App
$ npx react-native-init <project>
```

## Run IOS Simulator
**Requirements**
- Xcode

```
# Remove yarn stuff (optional)
$ rm -rf yarn.lock node_modules

$ npm install
$ npx react-native run-ios
```
